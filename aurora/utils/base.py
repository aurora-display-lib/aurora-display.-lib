
def has_flag(setting: int, flag: int):
    return setting & flag == flag
